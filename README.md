# Statistics of files

Count words, lines and characters in files of a certain folder

# Usage
Place file CTW.py in the folder where the files that you need to calculate their statistics are located, then run the following instruction from command line:

```
python CTW.py
```
and 
```
python CTW.py ar
```
or 
```
python CTW.py en 
```
if you need the name of the logfile to specify the language of the files that were processed.

