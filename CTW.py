from pathlib import Path
import os
import sys

def func(suffix=''):
    file_path=Path().resolve()
    files = os.listdir(file_path)
    original_stdout = sys.stdout
    log = open('Logs'+suffix, "w")
    sys.stdout = log
    print("Calculating for files in: "+os.path.abspath(file_path))
    n_o_lg = 0
    n_o_wg = 0
    n_o_cg = 0
    for file_n in files:
        if file_n =="CTW.py":
            continue
        file = open(file_n, "r")
        number_of_lines = 0
        number_of_words = 0
        number_of_characters = 0
        for line in file:
            line = line.strip("\n")
            words = line.split()
            number_of_lines += 1
            number_of_words += len(words)
            number_of_characters += len(line)
        file.close()
        n_o_lg += number_of_lines
        n_o_wg += number_of_words
        n_o_cg += number_of_characters
        print("Filename:",file_n)
        print("lines:", number_of_lines, "words:", number_of_words, "characters:", number_of_characters)
        print()
    print()
    print("Total files: "+str(len(files)-1),"Total lines: "+ str(n_o_lg), "Total words: "+ str(n_o_wg), "Total characters: "+ str(n_o_cg),sep='\n')
    log.close()
    sys.stdout = original_stdout


if __name__ == "__main__":
    if len(sys.argv) > 1:
        suffix = "_log_"+sys.argv[1].upper()+".txt"
    else:
        suffix = "_log.txt"
    func(suffix)
    
    
    
   